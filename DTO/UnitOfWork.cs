﻿using NodeData.Interfaces;
using NodeData.Models;
using NodeData.Repository;
using System;
using System.Data.Entity.Validation;
using System.Threading.Tasks;

namespace NodeData.DTO
{
    public class UnitOfWork : IUnitOfWork
    {
        #region members
        private readonly NodeDataContext _context;
        private readonly TransModContext _tContext;
        private bool disposed = false;
        private string _errorLog = string.Empty;
        #endregion

        #region Properties
        public string ErrorMessage
        {
            get
            {
                return _errorLog;
            }

            private set
            {
                _errorLog = value;
            }
        }

        #endregion


        public UnitOfWork(NodeDataContext context)
        {
            _context = context;
            AddRepos();
        }
        public UnitOfWork(TransModContext context)
        {
            _tContext = context;
            AddTransRepos();
        }

        private void AddTransRepos()
        {
            Addresses = new AddressRepository(_tContext);
            TransEnums = new EnumRepository(_tContext);
            TransEnumTypes = new EnumTypesRepository(_tContext);
            Images = new ImageRepository(_tContext);
            JobHeaders = new JobHeaderRepository(_tContext);
            JobNotes = new JobNoteRepository(_tContext);
            Organisations = new OrganisationRepository(_tContext);
            RunLinks = new RunLinkRepository(_tContext);
            RunSheets = new RunSheetRepository(_tContext);
            TransportLegs = new TransportLegRepository(_tContext);
        }

        public void AddRepos()
        {
            Customers = new CustomerRepository(_context);
            Profiles = new ProfileRepository(_context);
            TaskLists = new TaskListsRepository(_context);
            OutboundTaskLists = new OutboundTaskListsRepository(_context);
            CargowiseEnums = new Cargowise_EnumsRepository(_context);
            CargowiseContext = new CargowiseContextRepository(_context);
            Transactions = new TransactionRepository(_context);
            CustProfiles = new CustProfileRepository(_context);
            HeartBeats = new HeartBeatRepository(_context);
            ApiLogs = new ApiLogRepository(_context);
            DTServices = new DTSRepository(_context);
            Modules = new ModuleListingRepository(_context);
            CustomerModules = new CustomerModuleRepository(_context);
            MappingMasterFields = new MappingMasterFieldRepository(_context);
            MappingMasterOperations = new MappingMasterOperationRepository(_context);
            MapOperations = new MappingRepository(_context);
            MapHeaders = new Repository.MapHeaderRepository(_context);
            ToDos = new ToDoRepository(_context);
            TransLogs = new TransLogRepository(_context);
            ProcErrors = new ProcErrorRepository(_context);
            AppLogs = new AppLogRepository(_context);
            AppSettings = new AppSettingRepository(_context);
            ConversionProcesses = new ConversionProcessRepository(_context);
            ReferenceNumbers = new ReferenceNumbersRepository(_context);
            Orders = new OrderRepository(_context);
            OrderLines = new OrderlineRepository(_context);
        }

        public IAppSettingRepository AppSettings
        {
            get;
            private set;

        }
        public IAppLogRepository AppLogs
        {
            get;
            private set;
        }
        public IAddressRepository Addresses
        {
            get; private set;
        }

        public IEnumRepository TransEnums
        {
            get;
            private set;
        }

        public IEnumTypesRepository TransEnumTypes
        {
            get; private set;
        }

        public IImageRepository Images
        {
            get;
            private set;
        }

        public IJobHeaderRepository JobHeaders
        {
            get;
            private set;
        }

        public IJobNoteRepository JobNotes
        {
            get;
            private set;
        }

        public IOrganisationRepository Organisations
        {
            get;
            private set;
        }

        public IRunLinkRepository RunLinks
        {
            get;
            private set;

        }

        public IRunSheetRepository RunSheets
        {
            get;
            private set;
        }
        public ITransportLegRepository TransportLegs
        {
            get;
            private set;
        }

        public IProcErrorRepository ProcErrors
        {
            get;
            private set;
        }
        public ITransLogRepository TransLogs
        {
            get;
            private set;
        }
        public IToDoRepository ToDos
        {
            get;
            private set;
        }
        public IMapHeaderRepository MapHeaders
        {
            get;
            private set;
        }
        public IMappingRepository MapOperations
        {
            get;
            private set;
        }
        public IMappingMasterOperationRepository MappingMasterOperations
        {
            get;
            private set;
        }

        public IMappingMasterFieldRepository MappingMasterFields
        {
            get;
            private set;
        }

        public ICustomerModuleRepository CustomerModules
        {
            get;
            private set;
        }
        public IModuleListingRepository Modules
        {
            get;
            private set;
        }
        public IDTSRepository DTServices
        {
            get;
            private set;
        }
        public IApiLogRepository ApiLogs
        {
            get;
            private set;
        }

        public IHeartBeatRepository HeartBeats
        {
            get;
            private set;
        }

        public ICustProfileRepository CustProfiles
        {
            get;
            private set;
        }
        public ITransactionRepository Transactions
        {
            get;
            private set;
        }

        public ICargowiseContextRepository CargowiseContext
        {
            get;
            private set;
        }

        public ICargowise_EnumsRepository CargowiseEnums
        {
            get;
            private set;
        }
        public ITaskListsRepository TaskLists
        {
            get;
            private set;
        }

        public IOutboundTaskListsRepository OutboundTaskLists
        {
            get;
            private set;
        }

        public IProfileRepository Profiles
        {
            get;
            private set;
        }

        public ICustomerRepository Customers
        {
            get;
            private set;
        }

        public IConversionProcessRepository ConversionProcesses
        {
            get;
            private set;
        }

        public IReferenceNumbersRepository ReferenceNumbers
        {
            get;
            private set;
        }

        public IOrderRepository Orders
        {
            get;
            private set;
        }

        public IOrderLineRepository OrderLines
        {
            get;
            private set;
        }


        public bool DBExists()
        {
            bool exists = false;
            if (_context != null)
            {
                exists = _context.Database.Exists();
            }
            if (_tContext != null)
            {
                exists = _tContext.Database.Exists();
            }
            return exists;

        }

        public async Task<int> CompleteAsync()
        {
            if (_context != null)
            {
                try
                {
                    return await _context.SaveChangesAsync();
                }
                catch (InvalidOperationException ex)
                {
                    _errorLog += ex.GetType().Name + "Found: Error was: " + ex.Message + ":" + ex.InnerException.Message + Environment.NewLine;
                }
                catch (DbEntityValidationException ex)
                {

                    foreach (DbEntityValidationResult validationResult in ex.EntityValidationErrors)
                    {
                        string entityName = validationResult.Entry.Entity.GetType().Name;
                        foreach (DbValidationError error in validationResult.ValidationErrors)
                        {
                            _errorLog += entityName + "." + error.PropertyName + ": " + error.ErrorMessage + Environment.NewLine;

                        }
                    }
                }
            }
            if (_tContext != null)
            {
                try
                {
                    return await _tContext.SaveChangesAsync();
                }
                catch (InvalidOperationException ex)
                {
                    _errorLog += ex.GetType().Name + "Found: Error was: " + ex.Message + ":" + ex.InnerException.Message + Environment.NewLine;
                }
                catch (DbEntityValidationException ex)
                {

                    foreach (DbEntityValidationResult validationResult in ex.EntityValidationErrors)
                    {
                        string entityName = validationResult.Entry.Entity.GetType().Name;
                        foreach (DbValidationError error in validationResult.ValidationErrors)
                        {
                            _errorLog += entityName + "." + error.PropertyName + ": " + error.ErrorMessage + Environment.NewLine;

                        }
                    }
                }
            }

            return await Task.FromResult(-1);

        }
        public int Complete()
        {
            if (_context != null)
            {
                try
                {
                    return _context.SaveChanges();
                }
                catch (InvalidOperationException ex)
                {
                    _errorLog += ex.GetType().Name + "Found: Error was: " + ex.Message + ":" + ex.InnerException.Message + Environment.NewLine;
                }
                catch (DbEntityValidationException ex)
                {

                    foreach (DbEntityValidationResult validationResult in ex.EntityValidationErrors)
                    {
                        string entityName = validationResult.Entry.Entity.GetType().Name;
                        foreach (DbValidationError error in validationResult.ValidationErrors)
                        {
                            _errorLog += entityName + "." + error.PropertyName + ": " + error.ErrorMessage + Environment.NewLine;

                        }
                    }
                }
            }
            if (_tContext != null)
            {
                try
                {
                    return _tContext.SaveChanges();
                }
                catch (InvalidOperationException ex)
                {
                    _errorLog += ex.GetType().Name + "Found: Error was: " + ex.Message + ":" + ex.InnerException.Message + Environment.NewLine;
                }
                catch (DbEntityValidationException ex)
                {

                    foreach (DbEntityValidationResult validationResult in ex.EntityValidationErrors)
                    {
                        string entityName = validationResult.Entry.Entity.GetType().Name;
                        foreach (DbValidationError error in validationResult.ValidationErrors)
                        {
                            _errorLog += entityName + "." + error.PropertyName + ": " + error.ErrorMessage + Environment.NewLine;

                        }
                    }
                }
            }

            return -1;

        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_context != null)
                    _context.Dispose();
                if (_tContext != null)
                    _tContext.Dispose();
            }
        }

        public void Dispose()
        {
            if (!disposed)
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        }
    }
}