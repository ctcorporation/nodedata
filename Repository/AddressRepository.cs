﻿using NodeData.Interfaces;
using NodeData.Models;

namespace NodeData.Repository
{
    public class AddressRepository : GenericRepository<Address>, IAddressRepository
    {
        public AddressRepository(TransModContext context)
            : base(context)
        {


        }

        public TransModContext TransModContext
        {
            get
            {
                return TContext as TransModContext;
            }
        }

    }
}
