﻿using NodeData.Models;

namespace NodeData.Repository
{
    public class ModuleListingRepository : GenericRepository<ModuleListing>, IModuleListingRepository
    {
        public ModuleListingRepository(NodeDataContext context)
            : base(context)
        {


        }

        public NodeDataContext NodeDataContext
        {
            get { return Context as NodeDataContext; }
        }
    }
}
