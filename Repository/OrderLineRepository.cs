﻿using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;

namespace NodeData.Repository
{
    public class OrderlineRepository : GenericRepository<OrderlineRepository>, IOrderLineRepository
    {
        public OrderlineRepository(NodeDataContext context)
            : base(context)
        {

        }

        public NodeDataContext NodeDataContext
        {
            get
            {
                return Context as NodeDataContext;
            }
        }

        public void Add(CTCOrderline entity)
        {
            if (Context != null)
            {
                Context.Set<CTCOrderline>().Add(entity);
            }
            if (TContext != null)
            {
                TContext.Set<CTCOrderline>().Add(entity);
            }
        }


        public void AddRange(IEnumerable<CTCOrderline> entities)
        {
            if (Context != null)
            {
                Context.Set<CTCOrderline>().AddRange(entities);
            }
            if (TContext != null)
            {
                TContext.Set<CTCOrderline>().AddRange(entities);
            }

        }

        public CTCOrderline Get(Guid id)
        {
            CTCOrderline found = null;
            if (Context != null)
            {
                found = Context.Set<CTCOrderline>().Find(id);
            }
            if (TContext != null)
            {
                found = TContext.Set<CTCOrderline>().Find(id);
            }
            return found;

        }



        public IQueryable<CTCOrderline> Find(Expression<Func<CTCOrderline, bool>> predicate)
        {
            IQueryable<CTCOrderline> query = null;
            if (Context != null)
            {
                query = Context.Set<CTCOrderline>().Where(predicate);
            }
            if (TContext != null)
            {
                query = TContext.Set<CTCOrderline>().Where(predicate);
            }

            return query;

        }

        public IQueryable<CTCOrderline> Find(string filter, object[] paramsList)
        {
            IQueryable<CTCOrderline> query = null;
            if (Context != null)
            {
                query = Context.Set<CTCOrderline>().Where(filter, paramsList);
            }
            if (TContext != null)
            {
                query = TContext.Set<CTCOrderline>().Where(filter, paramsList);
            }
            return query;

        }

        public IQueryable<CTCOrderline> GetAll()
        {
            IQueryable<CTCOrderline> entity = null;
            if (Context != null)
            {
                entity = Context.Set<CTCOrderline>();
            }
            if (TContext != null)
            {
                entity = TContext.Set<CTCOrderline>();
            }
            return entity;
        }

        public void Remove(CTCOrderline entity)
        {
            if (Context != null)
            {
                Context.Set<CTCOrderline>().Remove(entity);
            }
            if (TContext != null)
            {
                TContext.Set<CTCOrderline>().Remove(entity);

            }

        }



        public void RemoveRange(IEnumerable<CTCOrderline> entities)
        {
            if (Context != null)
            {
                Context.Set<CTCOrderline>().RemoveRange(entities);
            }
            if (TContext != null)
            {
                TContext.Set<CTCOrderline>().RemoveRange(entities);
            }

        }
    }
}
