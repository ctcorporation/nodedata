﻿using NodeData.Models;

namespace NodeData.Repository
{
    public class CargowiseContextRepository : GenericRepository<CargowiseContext>, ICargowiseContextRepository
    {
        public CargowiseContextRepository(NodeDataContext context)
            : base(context)
        {

        }

        public NodeDataContext NodeDataContext
        {
            get
            {
                return Context as NodeDataContext;
            }
        }

    }
}
