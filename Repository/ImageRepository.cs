﻿using NodeData.Interfaces;
using NodeData.Models;

namespace NodeData.Repository
{
    public class ImageRepository : GenericRepository<Image>, IImageRepository
    {
        public ImageRepository(TransModContext context)
            : base(context)
        {

        }

        public TransModContext TransModContext
        {
            get
            { return TContext as TransModContext; }
        }
    }
}
