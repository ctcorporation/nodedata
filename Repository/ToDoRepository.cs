﻿using NodeData.Models;

namespace NodeData.Repository
{
    public class ToDoRepository : GenericRepository<ToDo>, IToDoRepository
    {
        public ToDoRepository(NodeDataContext context)
            : base(context)
        {

        }

        public NodeDataContext NodeDataContext
        {
            get
            {
                return Context as NodeDataContext;
            }
        }
    }
}