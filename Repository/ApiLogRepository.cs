﻿using NodeData.Models;

namespace NodeData.Repository
{
    public class ApiLogRepository : GenericRepository<ApiLog>, IApiLogRepository
    {
        public ApiLogRepository(NodeDataContext context)
            : base(context)
        {

        }

        public NodeDataContext NodeDataContext
        {
            get
            {
                return Context as NodeDataContext;
            }
        }
    }
}
