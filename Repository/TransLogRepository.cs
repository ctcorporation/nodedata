﻿using NodeData.Models;
namespace NodeData.Repository
{
    public class TransLogRepository : GenericRepository<TransactionLog>, ITransLogRepository
    {
        public TransLogRepository(NodeDataContext context)
            : base(context)
        {

        }

        public NodeDataContext NodeDataContext
        {
            get
            {
                return Context as NodeDataContext;
            }
        }
    }
}