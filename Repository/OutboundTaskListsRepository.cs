﻿using NodeData.Models;

namespace NodeData.Repository
{
    class OutboundTaskListsRepository : GenericRepository<OutboundTaskList>, IOutboundTaskListsRepository
    {
        public OutboundTaskListsRepository(NodeDataContext context)
            : base(context)
        {

        }

        public NodeDataContext NodeDataContext
        {
            get { return Context as NodeDataContext; }
        }
    }
}