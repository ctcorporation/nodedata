﻿using NodeData.Interfaces;
using NodeData.Models;

namespace NodeData.Repository
{
    public class EnumRepository : GenericRepository<Enums>, IEnumRepository
    {
        public EnumRepository(TransModContext context)
            : base(context)
        {

        }

        public TransModContext TransModContext
        {
            get
            {
                return TContext as TransModContext;
            }
        }
    }
}
