﻿using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;

namespace NodeData.Repository
{
    class ReferenceNumbersRepository : GenericRepository<ReferenceNumber>, IReferenceNumbersRepository
    {
        public ReferenceNumbersRepository(NodeDataContext context)
            : base(context)
        {

        }

        public NodeDataContext NodeDataContext
        {
            get { return Context as NodeDataContext; }
        }
    }
}
