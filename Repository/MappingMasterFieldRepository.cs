﻿using NodeData.Models;

namespace NodeData.Repository
{
    public class MappingMasterFieldRepository : GenericRepository<MappingMasterField>, IMappingMasterFieldRepository
    {
        public MappingMasterFieldRepository(NodeDataContext context)
            : base(context)
        {

        }

        private NodeDataContext NodeDataContext
        {
            get { return Context as NodeDataContext; }
        }
    }
}
