﻿using NodeData.Interfaces;
using NodeData.Models;

namespace NodeData.Repository
{
    public class RunSheetRepository : GenericRepository<RunSheet>, IRunSheetRepository
    {
        public RunSheetRepository(TransModContext context)
            : base(context)
        {


        }

        public TransModContext TransModContext
        {
            get
            {
                return TContext as TransModContext;
            }
        }
    }
}
