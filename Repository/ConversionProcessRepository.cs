﻿using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NodeData.Repository
{
    public class ConversionProcessRepository : GenericRepository<ConversionProcess>, IConversionProcessRepository
    {
        public ConversionProcessRepository(NodeDataContext context)
            : base(context)
        {

        }
        public NodeDataContext NodeDataContext
        {
            get { return Context as NodeDataContext; }
        }
    }
}
