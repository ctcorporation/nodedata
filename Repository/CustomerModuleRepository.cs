﻿using NodeData.Models;

namespace NodeData.Repository
{
    public class CustomerModuleRepository : GenericRepository<CustomerModuleLookup>, ICustomerModuleRepository
    {
        public CustomerModuleRepository(NodeDataContext context)
            : base(context)
        {

        }

        public NodeDataContext NodeDataContext
        {
            get { return Context as NodeDataContext; }
        }
    }
}
