﻿using NodeData.Models;

namespace NodeData.Repository
{
    public class ProcErrorRepository : GenericRepository<Processing_Error>, IProcErrorRepository
    {
        public ProcErrorRepository(NodeDataContext context)
        : base(context)
        {

        }

        public NodeDataContext NodeDataContext
        {
            get
            {
                return Context as NodeDataContext;
            }
        }
    }
}