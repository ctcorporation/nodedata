﻿using NodeData.Models;

namespace NodeData.Repository
{
    public class MappingRepository : GenericRepository<MapOperation>, IMappingRepository
    {
        public MappingRepository(NodeDataContext context)
            : base(context)
        {

        }

        private NodeDataContext NodeDataContext
        {
            get { return Context as NodeDataContext; }
        }
    }
}
