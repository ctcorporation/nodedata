﻿using NodeData.Models;

namespace NodeData.Repository
{
    public class DTSRepository : GenericRepository<DTS>, IDTSRepository
    {
        public DTSRepository(NodeDataContext context)
            : base(context)
        {

        }

        public NodeDataContext NodeDataContext
        {
            get
            {
                return Context as NodeDataContext;
            }
        }
    }
}
