﻿using NodeData.Interfaces;
using NodeData.Models;

namespace NodeData.Repository
{
    public class TransportLegRepository : GenericRepository<TransportLeg>, ITransportLegRepository

    {
        public TransportLegRepository(TransModContext context)
            : base(context)
        {

        }

        public TransModContext TransModContext
        {
            get
            { return TContext as TransModContext; }
        }
    }
}
