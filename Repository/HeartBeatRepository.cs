﻿using NodeData.Models;

namespace NodeData.Repository
{
    public class HeartBeatRepository : GenericRepository<HeartBeat>, IHeartBeatRepository
    {
        public HeartBeatRepository(NodeDataContext context)
            : base(context)
        {

        }

        public NodeDataContext NodeDataContext
        {
            get
            {
                return Context as NodeDataContext;
            }
        }

    }
}
