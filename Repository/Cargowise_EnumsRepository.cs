﻿using NodeData.Models;

namespace NodeData.Repository
{
    public class Cargowise_EnumsRepository : GenericRepository<Cargowise_Enums>, ICargowise_EnumsRepository
    {
        public Cargowise_EnumsRepository(NodeDataContext context)
            : base(context)
        {

        }

        public NodeDataContext NodeDataContext
        {
            get
            {
                return Context as NodeDataContext;
            }
        }

    }
}
