﻿using NodeData.Interfaces;
using NodeData.Models;

namespace NodeData.Repository
{
    public class RunLinkRepository : GenericRepository<RunLink>, IRunLinkRepository
    {
        public RunLinkRepository(TransModContext context)
            : base(context)
        {

        }

        public TransModContext TransModContext
        {
            get
            {
                return TContext as TransModContext;
            }
        }
    }
}
