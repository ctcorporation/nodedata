﻿using NodeData.Models;

namespace NodeData.Repository
{
    public class MappingMasterOperationRepository : GenericRepository<MappingMasterOperation>, IMappingMasterOperationRepository
    {
        public MappingMasterOperationRepository(NodeDataContext context)
            : base(context)
        {


        }

        private NodeDataContext NodeDataContext
        {
            get { return Context as NodeDataContext; }
        }

    }
}
