﻿using NodeData.Models;

namespace NodeData.Repository
{
    public class ProfileRepository : GenericRepository<Profile>, IProfileRepository
    {
        public ProfileRepository(NodeDataContext context)
            : base(context)
        {

        }

        public NodeDataContext NodeDataContext
        {
            get { return Context as NodeDataContext; }
        }
    }
}
