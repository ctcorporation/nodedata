﻿using NodeData.Models;

namespace NodeData.Repository
{
    public class AppLogRepository : GenericRepository<AppLog>, IAppLogRepository
    {
        public AppLogRepository(NodeDataContext context)
            : base(context)
        {

        }

        public NodeDataContext NodeDataContext
        {
            get
            {
                return Context as NodeDataContext;
            }
        }
    }
}
