﻿using NodeData.Models;

namespace NodeData.Repository
{
    class TaskListsRepository : GenericRepository<TaskList>, ITaskListsRepository
    {
        public TaskListsRepository(NodeDataContext context)
            : base(context)
        {

        }

        public NodeDataContext NodeDataContext
        {
            get { return Context as NodeDataContext; }
        }
    }
}
