﻿using NodeData.Interfaces;
using NodeData.Models;

namespace NodeData.Repository
{
    public class EnumTypesRepository : GenericRepository<EnumType>, IEnumTypesRepository
    {
        public EnumTypesRepository(TransModContext context)
            : base(context)
        {

        }

        public TransModContext TransModContext
        {
            get
            {
                return TContext as TransModContext;
            }
        }
    }
}
