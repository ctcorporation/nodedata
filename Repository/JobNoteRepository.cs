﻿using NodeData.Interfaces;
using NodeData.Models;

namespace NodeData.Repository
{
    public class JobNoteRepository : GenericRepository<JobNote>, IJobNoteRepository
    {
        public JobNoteRepository(TransModContext context)
            : base(context)
        {

        }

        public TransModContext TransModContext
        {
            get
            {
                return TContext as TransModContext;
            }
        }

    }
}
