﻿using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;

namespace NodeData.Repository
{
    public class OrderRepository : GenericRepository<OrderRepository>, IOrderRepository
    {
        public OrderRepository(NodeDataContext context)
            : base(context)
        {

        }

        public NodeDataContext NodeDataContext
        {
            get
            {
                return Context as NodeDataContext;
            }
        }

        public void Add(CTCOrder entity)
        {
            if (Context != null)
            {
                Context.Set<CTCOrder>().Add(entity);
            }
            if (TContext != null)
            {
                TContext.Set<CTCOrder>().Add(entity);
            }
        }


        public void AddRange(IEnumerable<CTCOrder> entities)
        {
            if (Context != null)
            {
                Context.Set<CTCOrder>().AddRange(entities);
            }
            if (TContext != null)
            {
                TContext.Set<CTCOrder>().AddRange(entities);
            }

        }

        public CTCOrder Get(Guid id)
        {
            CTCOrder found = null;
            if (Context != null)
            {
                found = Context.Set<CTCOrder>().Find(id);
            }
            if (TContext != null)
            {
                found = TContext.Set<CTCOrder>().Find(id);
            }
            return found;

        }



        public IQueryable<CTCOrder> Find(Expression<Func<CTCOrder, bool>> predicate)
        {
            IQueryable<CTCOrder> query = null;
            if (Context != null)
            {
                query = Context.Set<CTCOrder>().Where(predicate);
            }
            if (TContext != null)
            {
                query = TContext.Set<CTCOrder>().Where(predicate);
            }

            return query;

        }

        public IQueryable<CTCOrder> Find(string filter, object[] paramsList)
        {
            IQueryable<CTCOrder> query = null;
            if (Context != null)
            {
                query = Context.Set<CTCOrder>().Where(filter, paramsList);
            }
            if (TContext != null)
            {
                query = TContext.Set<CTCOrder>().Where(filter, paramsList);
            }
            return query;

        }

        public IQueryable<CTCOrder> GetAll()
        {
            IQueryable<CTCOrder> entity = null;
            if (Context != null)
            {
                entity = Context.Set<CTCOrder>();
            }
            if (TContext != null)
            {
                entity = TContext.Set<CTCOrder>();
            }
            return entity;
        }

        public void Remove(CTCOrder entity)
        {
            if (Context != null)
            {
                Context.Set<CTCOrder>().Remove(entity);
            }
            if (TContext != null)
            {
                TContext.Set<CTCOrder>().Remove(entity);

            }

        }



        public void RemoveRange(IEnumerable<CTCOrder> entities)
        {
            if (Context != null)
            {
                Context.Set<CTCOrder>().RemoveRange(entities);
            }
            if (TContext != null)
            {
                TContext.Set<CTCOrder>().RemoveRange(entities);
            }

        }
    }
}
