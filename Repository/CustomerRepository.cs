﻿using NodeData.Models;

namespace NodeData.Repository
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(NodeDataContext context)
            : base(context)
        {

        }


        public NodeDataContext NodeDataContext
        {
            get
            {
                return Context as NodeDataContext;

            }
        }
    }
}
