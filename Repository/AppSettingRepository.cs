﻿using NodeData.Models;

namespace NodeData.Repository
{
    public class AppSettingRepository : GenericRepository<AppSetting>, IAppSettingRepository
    {
        public AppSettingRepository(NodeDataContext context)
            : base(context)
        {

        }

        public NodeDataContext NodeDataContext
        {
            get
            {
                return Context as NodeDataContext;
            }

        }
    }
}
