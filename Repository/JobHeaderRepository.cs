﻿using NodeData.Interfaces;
using NodeData.Models;

namespace NodeData.Repository
{
    public class JobHeaderRepository : GenericRepository<JobHeader>, IJobHeaderRepository
    {
        public JobHeaderRepository(TransModContext context)
            : base(context)
        {

        }

        public TransModContext TransModContext
        {
            get
            {
                return TContext as TransModContext;
            }
        }
    }
}
