﻿using NodeData.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Linq.Expressions;

namespace NodeData.Repository
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected readonly NodeDataContext Context;
        protected readonly TransModContext TContext;


        public bool CheckConnection()
        {

            try
            {
                Context.Database.Connection.Open();
                Context.Database.Connection.Close();
                return true;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return false;
        }
        public GenericRepository(NodeDataContext context)
        {
            Context = context;
        }
        public GenericRepository(TransModContext context)
        {
            TContext = context;
        }
        public void Add(TEntity entity)
        {
            if (Context != null)
            {
                Context.Set<TEntity>().Add(entity);
            }
            if (TContext != null)
            {
                TContext.Set<TEntity>().Add(entity);
            }
        }


        public void AddRange(IEnumerable<TEntity> entities)
        {
            if (Context != null)
            {
                Context.Set<TEntity>().AddRange(entities);
            }
            if (TContext != null)
            {
                TContext.Set<TEntity>().AddRange(entities);
            }

        }

        public TEntity Get(Guid id)
        {
            TEntity found = null;
            if (Context != null)
            {
                found = Context.Set<TEntity>().Find(id);
            }
            if (TContext != null)
            {
                found = TContext.Set<TEntity>().Find(id);
            }
            return found;

        }



        public IQueryable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            IQueryable<TEntity> query = null;
            if (Context != null)
            {
                query = Context.Set<TEntity>().Where(predicate);
            }
            if (TContext != null)
            {
                query = TContext.Set<TEntity>().Where(predicate);
            }

            return query;

        }

        public IQueryable<TEntity> Find(string filter, object[] paramsList)
        {
            IQueryable<TEntity> query = null;
            if (Context != null)
            {
                query = Context.Set<TEntity>().Where(filter, paramsList);
            }
            if (TContext != null)
            {
                query = TContext.Set<TEntity>().Where(filter, paramsList);
            }
            return query;

        }

        public IQueryable<TEntity> GetAll()
        {
            IQueryable<TEntity> entity = null;
            if (Context != null)
            {
                entity = Context.Set<TEntity>();
            }
            if (TContext != null)
            {
                entity = TContext.Set<TEntity>();
            }
            return entity;
        }

        public void Remove(TEntity entity)
        {
            if (Context != null)
            {
                Context.Set<TEntity>().Remove(entity);
            }
            if (TContext != null)
            {
                TContext.Set<TEntity>().Remove(entity);

            }

        }



        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            if (Context != null)
            {
                Context.Set<TEntity>().RemoveRange(entities);
            }
            if (TContext != null)
            {
                TContext.Set<TEntity>().RemoveRange(entities);
            }

        }

    }


}
