﻿using NodeData.Models;

namespace NodeData.Repository
{
    public class MapHeaderRepository : GenericRepository<MapHeader>, IMapHeaderRepository
    {
        public MapHeaderRepository(NodeDataContext context)
            : base(context)
        {

        }

        private NodeDataContext NodeDataContext
        {
            get
            {
                return Context as NodeDataContext;
            }

        }
    }
}
