﻿using NodeData.Models;

namespace NodeData.Repository
{
    class CustProfileRepository : GenericRepository<vw_CustomerProfile>, ICustProfileRepository
    {
        public CustProfileRepository(NodeDataContext context)
            : base(context)
        {

        }

        public NodeDataContext NodeDataContext
        {
            get
            {
                return Context as NodeDataContext;
            }
        }
    }
}
