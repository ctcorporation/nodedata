namespace NodeData.Models
{
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.SqlServer;

    public class NodeDBConfiguration : DbConfiguration
    {

        public NodeDBConfiguration()
        {
            SetTransactionHandler(SqlProviderServices.ProviderInvariantName, () => new CommitFailureHandler());
            SetExecutionStrategy(SqlProviderServices.ProviderInvariantName, () => new SqlAzureExecutionStrategy());
        }


    }

    [DbConfigurationType(typeof(NodeDBConfiguration))]
    public partial class NodeDataContext : DbContext
    {

        public NodeDataContext(string connString)
            : base(connString)
        {

        }
        public NodeDataContext()
            : base("name=NodedataEntities")
        {
        }

        public virtual DbSet<Archive> Archives { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<DTS> DTS { get; set; }
        public virtual DbSet<Profile> PROFILEs { get; set; }
        public virtual DbSet<TaskList> TaskLists { get; set; }
        public virtual DbSet<OutboundTaskList> OutboundTaskLists { get; set; }
        public virtual DbSet<vw_CustomerProfile> vw_CustomerProfile { get; set; }
        public virtual DbSet<MapOperation> MapOperations { get; set; }
        public virtual DbSet<ReferenceNumber> ReferenceNumbers { get; set; }
        public virtual DbSet<Cargowise_Enums> Cargowise_Enums { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<HeartBeat> HeartBeats { get; set; }
        public virtual DbSet<ApiLog> ApiLogs { get; set; }
        public virtual DbSet<ModuleListing> Modules { get; set; }
        public virtual DbSet<CustomerModuleLookup> CustomerModules { get; set; }
        public virtual DbSet<MappingMasterField> MappingMasterFields { get; set; }
        public virtual DbSet<MappingMasterOperation> MappingMasterOperation { get; set; }
        public virtual DbSet<MapHeader> MapHeader { get; set; }
        public virtual DbSet<ToDo> ToDos { get; set; }
        public virtual DbSet<TransactionLog> TransactionLogs { get; set; }
        public virtual DbSet<AppLog> AppLogs { get; set; }
        public virtual DbSet<Processing_Error> ProcErrors { get; set; }
        public virtual DbSet<ConversionProcess> ConversionProcesses { get; set; }
        public virtual DbSet<AppSetting> AppSettings { get; set; }
        public virtual DbSet<CTCOrder> CTCOrders { get; set; }
        public virtual DbSet<CTCOrderline> CTCOrderlines { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            Database.SetInitializer<NodeDataContext>(null);
            modelBuilder.Entity<Cargowise_Enums>()
                           .Property(e => e.CW_ID)
                           .IsRequired()
                           .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);


            modelBuilder.Entity<Customer>()
                .Property(e => e.C_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_IS_ACTIVE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_ON_HOLD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_PATH)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_FTP_CLIENT)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_CODE)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_TRIAL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_SHORTNAME)
                .IsFixedLength();

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_SATELLITE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_INVOICED)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DTS>()
                .Property(e => e.D_FINALPROCESSING)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DTS>()
                .Property(e => e.D_FILETYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DTS>()
                .Property(e => e.D_DTSTYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DTS>()
                .Property(e => e.D_DTS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DTS>()
                .Property(e => e.D_SEARCHPATTERN)
                .IsUnicode(false);

            modelBuilder.Entity<DTS>()
                .Property(e => e.D_NEWVALUE)
                .IsUnicode(false);

            modelBuilder.Entity<DTS>()
                .Property(e => e.D_QUALIFIER)
                .IsUnicode(false);

            modelBuilder.Entity<DTS>()
                .Property(e => e.D_TARGET)
                .IsUnicode(false);

            modelBuilder.Entity<DTS>()
                .Property(e => e.D_CURRENTVALUE)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_REASONCODE)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_SERVER)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_USERNAME)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_PASSWORD)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_DELIVERY)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_PORT)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_PATH)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_DIRECTION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_LIBNAME)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_MESSAGETYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_RECIPIENTID)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_MSGTYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_CHARGEABLE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_BILLTO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_SENDERID)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_DTS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_EMAILADDRESS)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_ACTIVE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_SSL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_SENDEREMAIL)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_SUBJECT)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_FILETYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_MESSAGEDESCR)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_EVENTCODE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_CUSTOMERCOMPANYNAME)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_GROUPCHARGES)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_XSD)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_PARAMLIST)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_METHOD)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_NOTIFY)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_FORWARDWITHFLAGS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_CWAPPCODE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_OUTBOUNDTYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.C_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.C_CODE)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.C_IS_ACTIVE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.C_ON_HOLD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.C_PATH)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.C_FTP_CLIENT)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_REASONCODE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_SERVER)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_USERNAME)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_PASSWORD)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_PORT)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_DELIVERY)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_PATH)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_DIRECTION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_XSD)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_LIBNAME)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_RECIPIENTID)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_SENDERID)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_DTS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_BILLTO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_CHARGEABLE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_MSGTYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_MESSAGETYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_ACTIVE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_FILETYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_EMAILADDRESS)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_SSL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_SENDEREMAIL)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_SUBJECT)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_MESSAGEDESCR)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_EVENTCODE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_CUSTOMERCOMPANYNAME)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.C_SHORTNAME)
                .IsFixedLength();

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_METHOD)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_PARAMLIST)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_NOTIFY)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_FORWARDWITHFLAGS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_CWAPPCODE)
                .IsFixedLength()
                .IsUnicode(false);


        }
    }
}
