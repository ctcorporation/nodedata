﻿namespace NodeData.Models
{
    using System.Data.Entity;

    public partial class SatelliteDataContext : DbContext
    {

        public SatelliteDataContext(string connString)
            : base(connString)
        {

        }
        public SatelliteDataContext()
            : base("name=SatelliteDataEntities")
        {
        }

        public virtual DbSet<Archive> Archives { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<DT> DTS { get; set; }
        public virtual DbSet<Profile> PROFILEs { get; set; }
        public virtual DbSet<TaskList> TaskLists { get; set; }
        public virtual DbSet<vw_CustomerProfile> vw_CustomerProfile { get; set; }
        public virtual DbSet<MapOperation> MapOperations { get; set; }
        public virtual DbSet<Cargowise_Enums> Cargowise_Enums { get; set; }
        public virtual DbSet<Transaction> Transactions { get; set; }
        public virtual DbSet<HeartBeat> HeartBeats { get; set; }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            Database.SetInitializer<NodeDataContext>(null);
            modelBuilder.Entity<Cargowise_Enums>()
                           .Property(e => e.CW_ID)
                           .IsRequired()
                           .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);


            modelBuilder.Entity<Customer>()
                .Property(e => e.C_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_IS_ACTIVE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_ON_HOLD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_PATH)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_FTP_CLIENT)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_CODE)
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_TRIAL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_SHORTNAME)
                .IsFixedLength();

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_SATELLITE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Customer>()
                .Property(e => e.C_INVOICED)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DT>()
                .Property(e => e.D_FINALPROCESSING)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DT>()
                .Property(e => e.D_FILETYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DT>()
                .Property(e => e.D_DTSTYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DT>()
                .Property(e => e.D_DTS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DT>()
                .Property(e => e.D_SEARCHPATTERN)
                .IsUnicode(false);

            modelBuilder.Entity<DT>()
                .Property(e => e.D_NEWVALUE)
                .IsUnicode(false);

            modelBuilder.Entity<DT>()
                .Property(e => e.D_QUALIFIER)
                .IsUnicode(false);

            modelBuilder.Entity<DT>()
                .Property(e => e.D_TARGET)
                .IsUnicode(false);

            modelBuilder.Entity<DT>()
                .Property(e => e.D_CURRENTVALUE)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_REASONCODE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_SERVER)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_USERNAME)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_PASSWORD)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_DELIVERY)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_PORT)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_PATH)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_DIRECTION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_LIBNAME)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_MESSAGETYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_RECIPIENTID)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_MSGTYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_CHARGEABLE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_BILLTO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_SENDERID)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_DTS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_EMAILADDRESS)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_ACTIVE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_SSL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_SENDEREMAIL)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_SUBJECT)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_FILETYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_MESSAGEDESCR)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_EVENTCODE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_CUSTOMERCOMPANYNAME)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_GROUPCHARGES)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_XSD)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_PARAMLIST)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_METHOD)
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_NOTIFY)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_FORWARDWITHFLAGS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Profile>()
                .Property(e => e.P_CWAPPCODE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.C_NAME)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.C_CODE)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.C_IS_ACTIVE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.C_ON_HOLD)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.C_PATH)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.C_FTP_CLIENT)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_REASONCODE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_SERVER)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_USERNAME)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_PASSWORD)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_DESCRIPTION)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_PORT)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_DELIVERY)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_PATH)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_DIRECTION)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_XSD)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_LIBNAME)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_RECIPIENTID)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_SENDERID)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_DTS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_BILLTO)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_CHARGEABLE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_MSGTYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_MESSAGETYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_ACTIVE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_FILETYPE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_EMAILADDRESS)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_SSL)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_SENDEREMAIL)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_SUBJECT)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_MESSAGEDESCR)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_EVENTCODE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_CUSTOMERCOMPANYNAME)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.C_SHORTNAME)
                .IsFixedLength();

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_METHOD)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_PARAMLIST)
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_NOTIFY)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_FORWARDWITHFLAGS)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<vw_CustomerProfile>()
                .Property(e => e.P_CWAPPCODE)
                .IsFixedLength()
                .IsUnicode(false);
        }
    }
}
