namespace NodeData.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("RunLink")]
    public partial class RunLink
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid RL_ID { get; set; }

        public Guid? RL_TP { get; set; }

        public Guid? RL_TD { get; set; }

        [StringLength(10)]
        public string RL_STATUS { get; set; }

        [StringLength(255)]
        public string RL_NOTES { get; set; }

        public Guid? RL_RS { get; set; }

        public int? RL_SEQUENCE { get; set; }
    }
}
