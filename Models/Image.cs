namespace NodeData.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Image")]
    public partial class Image
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid I_ID { get; set; }

        public Guid? I_REF { get; set; }

        [StringLength(1)]
        public string I_ReftTable { get; set; }

        public byte[] I_Image { get; set; }

        public virtual JobNote JobNote { get; set; }
    }
}
