﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NodeData.Models
{
    public class ReferenceNumber
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid REF_ID { get; set; }

        public string REF_HOUSEBILL { get; set; }

        public string REF_MASTERBILL { get; set; }

        public string REF_JOBNUMBER { get; set; }
    }
}
