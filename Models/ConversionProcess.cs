﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NodeData.Models
{
    public class ConversionProcess
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

        public Guid CP_ID { get; set; }

        [StringLength(1)]
        //convertion method from - to
        public string CP_Direction { get; set; }

        [StringLength(30)]
        public string CP_MethodName { get; set; }

        [StringLength(100)]
        public string CP_MethodDescription { get; set; }

    }
}
