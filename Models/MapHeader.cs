﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NodeData.Models
{
    public class MapHeader
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid MH_ID { get; set; }
        [Display(Name = "Map Name")]
        public string MH_Name { get; set; }
        [Display(Name = "Description of Map")]
        public string MH_Description { get; set; }

        public Guid MH_MO { get; set; }
        [MaxLength(20)]
        [Display(Name = "Sender or Owner")]
        public string MH_Customer { get; set; }
        [MaxLength(20)]
        [Display(Name = "Recipient or Cargowise customer")]
        public string MH_Recipient { get; set; }

    }
}
