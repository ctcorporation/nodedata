﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NodeData.Models
{
    public class CTCOrderline
    {
        [Key]
        public int OrderLineId { get; set; }
        public string SKU { get; set; }
        public int Qty { get; set; }
        public long OrderId { get; set; }
        public string ProductName { get; set; }
        public string CouponCode { get; set; }
        public decimal Price { get; set; }
        public decimal SubTotal { get; set; }
        public string WarehouseName { get; set; }
        public string OrderLineStatus { get; set; }
        public string OrderlineType { get; set; }
        public string ParentOrderlineItemSKU { get; set; }
        public bool DangerousGoods { get; set; }
    }
}
