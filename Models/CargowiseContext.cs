﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NodeData.Models
{
    public class CargowiseContext
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CC_ID { get; set; }

        [Column("CC_Context", TypeName = "varchar")]
        [MaxLength(20)]
        public string CC_Context { get; set; }

        [Column("CC_Description", TypeName = "varchar")]
        [MaxLength(10)]
        public string CC_Description { get; set; }

        [Column("CC_Type", TypeName = "varchar")]
        [MaxLength(20)]
        public string CC_Type { get; set; }
    }

}