namespace NodeData.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("TransportLeg")]
    public partial class TransportLeg
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TransportLeg()
        {
            JobNotes = new HashSet<JobNote>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid T_ID { get; set; }

        public Guid? T_J { get; set; }

        [StringLength(13)]
        public string T_ContainerNo { get; set; }

        public int? T_Pieces { get; set; }

        public decimal? T_Weight { get; set; }

        public decimal? T_Volume { get; set; }

        //public string T_ItemDescription { get; set; }

        //public int? T_ItemQuantity { get; set; }

        public Guid? T_A { get; set; }

        public DateTime? T_TimeStart { get; set; }

        public DateTime? T_TimeFin { get; set; }

        [StringLength(3)]
        public string T_DropMode { get; set; }

        [StringLength(3)]
        public string T_Containermode { get; set; }

        [StringLength(10)]
        public string T_JobType { get; set; }

        public int? T_PiecesDelivered { get; set; }

        public int? T_Status { get; set; }

        public int? T_LegNo { get; set; }

        public int? T_Priority { get; set; }

        [StringLength(1)]
        public string T_IsLoaded { get; set; }

        public DateTime? T_RequiredDate { get; set; }

        [StringLength(128)]
        public string T_DriverId { get; set; }

        [StringLength(3)]
        public string T_UOM { get; set; }

        public int? T_Sequence { get; set; }

        public byte[] T_Signature { get; set; }

        [StringLength(200)]
        public string T_SignedBy { get; set; }

        public Guid? T_A_Pick { get; set; }

        public Guid? T_A_Delivery { get; set; }

        public string T_Reference { get; set; }

        public int T_Type { get; set; }


        public virtual Address Address { get; set; }

        public virtual Enums Enum { get; set; }

        public virtual JobHeader JobHeader { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<JobNote> JobNotes { get; set; }
    }
}
