namespace NodeData.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("TRANSACTION_LOG")]
    public partial class TransactionLog
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid X_ID { get; set; }

        public Guid? X_P { get; set; }

        [StringLength(150)]
        public string X_FILENAME { get; set; }

        public DateTime? X_DATE { get; set; }

        [Required]
        [StringLength(1)]
        public string X_SUCCESS { get; set; }

        public Guid? X_C { get; set; }

        [StringLength(120)]
        public string X_LASTRESULT { get; set; }
    }
}
