﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NodeData.Models
{
    public class MappingMasterField
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid MM_ID { get; set; }

        [MaxLength(50)]
        [Display(Name = "Field Name")]
        public string MM_FieldName { get; set; }

        public Guid MM_MO { get; set; }
        [MaxLength(4)]
        [Display(Name = "Field Type")]
        public string MM_FieldType { get; set; }

    }
}
