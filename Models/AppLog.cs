﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NodeData.Models
{
    public class AppLog
    {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid GL_ID { get; set; }

        [MaxLength(50)]
        [Display(Name = "Application")]
        public string GL_AppName { get; set; }

        [MaxLength(50)]
        [Display(Name = "Server Name")]
        public string GL_ServerName { get; set; }

        [Display(Name = "Proces Date")]
        public DateTime GL_Date { get; set; }

        [MaxLength(50)]
        [Display(Name = "Process Name")]
        public string GL_ProcName { get; set; }

        [MaxLength(20)]
        [Display(Name = "MessageType")]
        public string GL_MessageType { get; set; }

        [Display(Name = "Level")]
        public int GL_MessageLevel { get; set; }

        [Display(Name = "Message Detail")]
        public string GL_MessageDetail { get; set; }

        [Display(Name = "File Path")]
        [MaxLength(150)]
        public string GL_Path { get; set; }






    }
}
