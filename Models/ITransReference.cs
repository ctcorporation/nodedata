﻿namespace NodeData.Models
{
    public interface ITransReference
    {
        RefType Ref1Type { get; set; }
        RefType Ref2Type { get; set; }
        RefType Ref3Type { get; set; }
        string Reference1 { get; set; }
        string Reference2 { get; set; }
        string Reference3 { get; set; }
    }
}