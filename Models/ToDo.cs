﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NodeData.Models
{

    [Table("TODO")]
    public partial class ToDo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public System.Guid L_ID { get; set; }
        public Nullable<System.Guid> L_P { get; set; }
        public string L_FILENAME { get; set; }
        public string L_LASTRESULT { get; set; }
        public Nullable<System.DateTime> L_DATE { get; set; }
    }


}