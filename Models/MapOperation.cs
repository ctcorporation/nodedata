﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NodeData.Models
{

    public class MapOperation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid MD_ID { get; set; }

        [MaxLength(100)]
        [Required]
        public string MD_MapDescription { get; set; }

        [MaxLength(50)]
        [Display(Name = "Map From")]
        public string MD_FromField { get; set; }

        [MaxLength(50)]
        [Display(Name = "Map To")]
        public string MD_ToField { get; set; }
        [MaxLength(3)]
        [Display(Name = "Data Type")]
        public string MD_DataType { get; set; }

        public Guid? MD_MH { get; set; }







    }
}
