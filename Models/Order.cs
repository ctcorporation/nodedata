﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NodeData.Models
{
    public class Orders
    {
        [Key]
        [Display(Name = "ID")]
        public long OrderId { get; set; }

        [Display(Name = "First Name")]
        public string ShipFirstName { get; set; }

        [Display(Name = "Last Name")]
        public string ShipLastName { get; set; }

        [Display(Name = "Company")]
        public string ShipCompanyName { get; set; }

        [Display(Name = "Address 1")]
        [Required]
        public string ShipAddress1 { get; set; }

        [Display(Name = "Address 2")]
        public string ShipAddress2 { get; set; }

        [Display(Name = "City")]
        public string ShipCity { get; set; }

        [Display(Name = "State")]
        public string ShipState { get; set; }

        [Display(Name = "Postal Code")]
        public string ShipPostalCode { get; set; }

        [Display(Name = "Country")]
        [Required]
        public string ShipCountry { get; set; }

        [Display(Name = "Phone Number")]
        public string ShipPhoneNumber { get; set; }

        [Display(Name = "Order Date")]
        public System.DateTime OrderDate { get; set; }


        [Display(Name = "Notes")]
        public string OrderNotes { get; set; }


        [Display(Name = "Amount")]
        public Nullable<decimal> Amount { get; set; }

        [Display(Name = "Email")]
        //[EmailAddress]
        public string CustomerEmail { get; set; }

        [Display(Name = "First Name")]
        public string BillingFirstName { get; set; }

        [Display(Name = "Last Name")]
        public string BillingLastName { get; set; }

        [Display(Name = "Phone Number")]
        public string BillingPhoneNumber { get; set; }

        [Display(Name = "Fax Number")]
        public string BillingFaxNumber { get; set; }

        [Display(Name = "Address 1")]
        public string BillingAddress1 { get; set; }

        [Display(Name = "Address 2")]
        public string BillingAddress2 { get; set; }

        [Display(Name = "City")]
        public string BillingCity { get; set; }

        [Display(Name = "Country")]
        public string BillingCountry { get; set; }

        [Display(Name = "State")]
        public string BillingState { get; set; }

        [Display(Name = "Postal Code")]
        public string BillingPostalCode { get; set; }

        [Display(Name = "Company")]
        public string BillingCompanyName { get; set; }

        [Display(Name = "Leave Parcel Message")]
        public string LeaveParcelMessage { get; set; }

        [Display(Name = "Gift Note")]
        public string GiftNote { get; set; }
        [Display(Name = "Coupon Code")]
        public string CouponCode { get; set; }
        [Display(Name = "Coupon Amount")]
        public decimal? CouponAmount { get; set; }

        [Display(Name = "Store")]
        public int StoreId { get; set; }

        [Display(Name = "Shipping Method")]
        public string ShippingMethodName { get; set; }

        [Display(Name = "Date Imported")] // no need to put [Required] as it's initialized in the Constructor
        public DateTime DateImported { get; set; }

        [Display(Name = "Last Updated")]
        [Required]
        public DateTime LastUpdated { get; set; }

        //[Display(Name = "Carrier")]
        //public Nullable<int> CarrierCodeId { get; set; }

        public string TrackingNo { get; set; }
        [Display(Name = "Store Order Id")]
        public string StoreOrderId { get; set; }
        [Display(Name = "Warehouse Order Id")]
        public string WarehouseOrderId { get; set; }

        public string ChannelId { get; set; }
        public Nullable<bool> ATL { get; set; }

        [Display(Name = "Debtor Code")]
        public string ExternalCustomerId { get; set; }

        public string WebUsername { get; set; }
        [Display(Name = "Shipping Total")]
        public decimal? ShippingTotal { get; set; }

        [Display(Name = "PO Number")]
        public string PurchaseOrderNumber { get; set; }

        [Display(Name = "External Warehouse ID")]
        public string ExternalWarehouseId { get; set; }
        public Nullable<DateTime> DateExportedToWarehouse { get; set; }
        public Nullable<DateTime> DateCompleted { get; set; }
        public Nullable<DateTime> DateExportedToAccounts { get; set; }
        public Nullable<DateTime> DateAccountsCompleted { get; set; }
        public bool? ValidOrder { get; set; }
        public string ValidOrderMessage { get; set; }
        public string EnteredBy { get; set; }

        [Display(Name = "Shipping Note")]
        public string ShippingNote { get; set; }
        public bool DangerousGoods { get; set; }
        public string UserId { get; set; }
        public bool Urgent { get; set; }
        public int? StorePluginId { get; set; }
        public string SalesPerson { get; set; }
        public DateTime? ShipDate { get; set; }
        public DateTime? ExpectedDeliveryDate { get; set; }
        public string GeoCodeXML { get; set; }
        public string PaymentTransactionId { get; set; }
        //public virtual CarrierCode CarrierCode { get; set; }
        //public virtual Store Store { get; set; }
        //public virtual ICollection<OrderLines> OrderLines { get; set; }
    }
}
