﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace NodeData.Models
{
    public class IdentityModels
    {

        public class ApplicationUser : IdentityUser
        {

            [DisplayFormat(DataFormatString = "{0:g}")]
            [Display(Name = "Last Logged In")]
            public DateTime? LastLoginTime { get; set; }
            [Display(Name = "Is Logged in")]
            public bool IsLoggedIn { get; set; }
            [MaxLength(20)]
            [Display(Name = "Company Code")]
            public string CompanyCode { get; set; }

            [Display(Name = "Is Active")]
            public bool IsActive { get; set; }

            [Display(Name = "Account Locked")]
            public bool IsLockedOut { get; set; }
            [MaxLength(50)]
            [Display(Name = "First Name")]
            public string FirstName { get; set; }
            [MaxLength(50)]
            [Display(Name = "Last Name")]
            public string LastName { get; set; }
            public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
            {
                // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
                var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
                // Add custom user claims here
                return userIdentity;
            }
        }

        public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
        {

            public ApplicationDbContext()
                : base("CommonEntity", throwIfV1Schema: false)
            {
            }

            public static ApplicationDbContext Create()
            {
                return new ApplicationDbContext();
            }


        }
    }
}
