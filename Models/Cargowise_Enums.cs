﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NodeData.Models
{
    public class Cargowise_Enums
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid CW_ID { get; set; }

        [Column("CW_ENUMTYPE", TypeName = "char")]
        [MaxLength(20)]
        public string CW_ENUMTYPE { get; set; }

        [Column("CW_ENUM", TypeName = "nchar")]
        [MaxLength(10)]
        public string CW_ENUM { get; set; }

        [Column("CW_MAPVALUE", TypeName = "varchar")]
        [MaxLength(20)]
        public string CW_MAPVALUE { get; set; }
    }

}
