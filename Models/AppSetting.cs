﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NodeData.Models
{
    public class AppSetting
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid AS_Id
        {
            get; set;
        }

        [MaxLength(50)]
        public string AS_AppName
        {
            get; set;
        }


        public string AS_AcceptedFiles
        {
            get; set;
        }

        public string AS_RejectFiles
        {
            get; set;
        }

        public string AS_RejectLocation
        {
            get; set;
        }

        public string AS_ArchiveLocation
        {
            get; set;
        }

        public int AS_ArchiveDays
        {
            get; set;
        }

    }
}
