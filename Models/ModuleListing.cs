﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NodeData.Models
{
    public class ModuleListing
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid MO_ID { get; set; }

        [MaxLength(20)]
        public string MO_ModuleName { get; set; }

        public bool MO_Chargeable { get; set; }

        [MaxLength(100)]
        public string MO_ModuleDescription { get; set; }


        public IList<CustomerModuleLookup> MO_MC { get; set; }

    }
}
