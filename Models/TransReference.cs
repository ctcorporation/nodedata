﻿

namespace NodeData.Models
{
    public class TransReference : ITransReference
    {
        private string reference1;
        private string reference2;
        private string reference3;
        private RefType ref1type;
        private RefType ref2type;
        private RefType ref3type;
        public string Reference1
        {
            get { return this.reference1; }
            set { this.reference1 = value; }
        }
        public string Reference2
        {
            get { return this.reference2; }
            set { this.reference2 = value; }
        }
        public string Reference3
        {
            get { return this.reference3; }
            set { this.reference3 = value; }
        }
        public RefType Ref1Type
        {
            get { return this.ref1type; }
            set { this.ref1type = value; }
        }
        public RefType Ref2Type
        {
            get { return this.ref2type; }
            set { this.ref2type = value; }
        }
        public RefType Ref3Type
        {
            get { return this.ref3type; }
            set { this.ref3type = value; }
        }


    }
}
