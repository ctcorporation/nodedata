﻿namespace NodeData.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class OutboundTaskList
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid OTL_ID { get; set; }

        [StringLength(100)]
        public string OTL_OrginalFileName { get; set; }

        [StringLength(50)]
        public string OTL_FileName { get; set; }

        public DateTime? OTL_ReceiveDate { get; set; }

        public bool OTL_Processed { get; set; }

        [StringLength(150)]
        public string OTL_Path { get; set; }

        [StringLength(100)]
        public string OTL_Subject { get; set; }

        [StringLength(50)]
        public string OTL_SenderID { get; set; }

        [StringLength(50)]
        public string OTL_RecipientID { get; set; }

        [StringLength(150)]
        public string OTL_ReceivedFolder { get; set; }

        [StringLength(10)]
        public string OTL_ReceiverProcess { get; set; }

        public DateTime? OTL_ProcessDate { get; set; }

        public Guid? OTL_P { get; set; }

        [StringLength(80)]
        public string OTL_CWFileName { get; set; }
        [StringLength(50)]
        public string OTL_FromAddress { get; set; }

        [StringLength(50)]
        public string OTL_ToAddress { get; set; }

        [StringLength(200)]
        public string OTL_ArchiveName { get; set; }

        [StringLength(3)]
        public string OTL_AppCode { get; set; }


    }
}
