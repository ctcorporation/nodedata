﻿namespace NodeData.Models

{
    public enum RefType
    {
        Master,

        Housebill,

        Order,

        Consol,

        Shipment,

        Container,

        Invoice,

        Brokerage,

        Transaction,

        Operation,

        WHSPick,

        WHSOrder,

        Product,

        Custom,

        Response,

        AgentRef,

        Event,

        Error,

        FileName,
        Conversion,

        Transport
    }
}
