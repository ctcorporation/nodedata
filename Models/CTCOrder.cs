﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NodeData.Models
{
    public class CTCOrder
    {
        public CTCOrder()
        {
            this.CTCOrderlines = new List<CTCOrderline>();
        }
        [Key]
        public long OrderId { get; set; }
        public string ShipFirstName { get; set; }
        public string ShipLastName { get; set; }
        public string ShipCompanyName { get; set; }
        public string ShipAddress1 { get; set; }
        public string ShipAddress2 { get; set; }
        public string ShipCity { get; set; }
        public string ShipState { get; set; }
        public string ShipPostalCode { get; set; }
        public string ShipCountry { get; set; }
        public string ShipPhoneNumber { get; set; }
        public System.DateTime OrderDate { get; set; }
        public string OrderNotes { get; set; }
        public Nullable<decimal> Amount { get; set; }
        public string CustomerEmail { get; set; }
        public string BillingFirstName { get; set; }
        public string BillingLastName { get; set; }
        public string BillingPhoneNumber { get; set; }
        public string BillingFaxNumber { get; set; }
        public string BillingAddress1 { get; set; }
        public string BillingAddress2 { get; set; }
        public string BillingCity { get; set; }
        public string BillingCountry { get; set; }
        public string BillingState { get; set; }
        public string BillingPostalCode { get; set; }
        public string BillingCompanyName { get; set; }
        public string LeaveParcelMessage { get; set; }
        public string GiftNote { get; set; }
        public string CouponCode { get; set; }
        public decimal? CouponAmount { get; set; }
        public string ShippingMethodName { get; set; }
        public string StoreOrderId { get; set; }
        public string WarehouseOrderId { get; set; }
        public decimal? ShippingTotal { get; set; }
        public string PurchaseOrderNumber { get; set; }
        public string ShippingNote { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerCity { get; set; }
        public string CustomerState { get; set; }
        public string CustomerCountry { get; set; }
        public string CustomerPostCode { get; set; }
        public string CustomerPhone { get; set; }

        public virtual ICollection<CTCOrderline> CTCOrderlines { get; set; }
    }
}
