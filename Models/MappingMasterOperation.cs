﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NodeData.Models
{
    public class MappingMasterOperation
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid MO_ID { get; set; }

        [MaxLength(50)]
        [Display(Name = "Map Operation")]
        public string MO_OperationName { get; set; }

        [Display(Name = "Operation Description")]
        public string MO_Description { get; set; }
        [MaxLength(50)]
        public string MD_MethodName { get; set; }

    }
}
