﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NodeData.Models
{
    public class CustomerModuleLookup
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid MC_ID { get; set; }

        [ForeignKey("CustomerCustomerModuleLookup")]
        public Guid? MC_C { get; set; }
        public virtual Customer CustomerCustomerModuleLookup { get; set; }
        [ForeignKey("ModuleListingCustomerModuleLookup")]
        public Guid? MC_MO { get; set; }
        public virtual ModuleListing ModuleListingCustomerModuleLookup { get; set; }
    }
}
