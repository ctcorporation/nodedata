﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace NodeData.Models
{
    public class OrderLines
    {
        [Key]
        public int OrderLineId { get; set; }

        [Required]
        public string SKU { get; set; }

        [Required]
        public int Qty { get; set; }

        [Display(Name = "Order")]
        [Required]
        public long OrderId { get; set; }

        [Display(Name = "Product")]
        [Required]
        public string ProductName { get; set; }

        [Display(Name = "ProductId")]
        public long? ProductId { get; set; }

        public string CouponCode { get; set; }

        public string Style { get; set; }
        public string Colour { get; set; }
        public string Size { get; set; }

        public decimal Price { get; set; }

        public decimal SubTotal { get; set; }

        public string StoreOrderLineId { get; set; }
        [Display(Name = "ParentId")]
        public string ParentStoreOrderLineId { get; set; }

        public int? WarehouseId { get; set; }
        [Display(Name = "WarehouseName")]
        public string WarehouseName { get; set; }
        public string EnteredBy { get; set; }
        //public OrderLineStatus OrderLineStatus { get; set; }
        //public OrderlineType OrderlineType { get; set; }
        public string ParentOrderlineItemSKU { get; set; }
        public bool DangerousGoods { get; set; }
        public virtual Orders Order { get; set; }
    }
}
