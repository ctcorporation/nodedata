﻿namespace NodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class updatevwcustprofile : DbMigration
    {
        public override void Up()
        {
            //AddColumn("dbo.vw_CustomerProfile", "P_IGNOREMESSAGE", c => c.String(maxLength: 1));
            //AddColumn("dbo.vw_CustomerProfile", "P_NOTES", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.vw_CustomerProfile", "P_NOTES");
            DropColumn("dbo.vw_CustomerProfile", "P_IGNOREMESSAGE");
        }
    }
}
