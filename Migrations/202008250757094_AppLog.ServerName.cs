﻿namespace NodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AppLogServerName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AppLogs", "GL_ServerName", c => c.String(maxLength: 50));
            AlterColumn("dbo.AppLogs", "GL_AppName", c => c.String(maxLength: 50));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.AppLogs", "GL_AppName", c => c.String(maxLength: 20));
            DropColumn("dbo.AppLogs", "GL_ServerName");
        }
    }
}
