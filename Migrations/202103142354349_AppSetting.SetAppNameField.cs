﻿namespace NodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AppSettingSetAppNameField : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AppSettings", "AS_AppName", c => c.String(maxLength: 50));
            DropColumn("dbo.AppSettings", "AS_AppKey");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AppSettings", "AS_AppKey", c => c.Guid(nullable: false));
            DropColumn("dbo.AppSettings", "AS_AppName");
        }
    }
}
