﻿// <auto-generated />
namespace NodeData.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.3.0")]
    public sealed partial class profilesaddprofilenotes : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(profilesaddprofilenotes));
        
        string IMigrationMetadata.Id
        {
            get { return "202103290350401_profiles.add.profilenotes"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
