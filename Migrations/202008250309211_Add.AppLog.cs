﻿namespace NodeData.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddAppLog : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AppLogs",
                c => new
                {
                    GL_ID = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),
                    GL_AppName = c.String(maxLength: 20),
                    GL_Date = c.DateTime(nullable: false),
                    GL_ProcName = c.String(maxLength: 50),
                    GL_MessageType = c.String(maxLength: 20),
                    GL_MessageLevel = c.Int(nullable: false),
                    GL_MessageDetail = c.String(),
                    GL_Path = c.String(maxLength: 150),
                })
                .PrimaryKey(t => t.GL_ID);

        }

        public override void Down()
        {
            DropTable("dbo.AppLogs");
        }
    }
}
