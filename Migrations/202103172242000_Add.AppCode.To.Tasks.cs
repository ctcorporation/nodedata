﻿namespace NodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAppCodeToTasks : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TaskLists", "TL_AppCode", c => c.String(maxLength: 3));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TaskLists", "TL_AppCode");
        }
    }
}
