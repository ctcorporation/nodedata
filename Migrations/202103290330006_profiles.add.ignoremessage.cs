﻿namespace NodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class profilesaddignoremessage : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PROFILE", "P_IGNOREMESSAGE", c => c.String(maxLength: 1));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PROFILE", "P_IGNOREMESSAGE");
        }
    }
}
