﻿namespace NodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class NewConversionTableaddition : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ConversionProcesses",
                c => new
                    {
                        CP_ID = c.Guid(nullable: false, identity: true, defaultValueSql:"newid()"),
                        CP_Direction = c.String(maxLength: 1),
                        CP_MethodName = c.String(maxLength: 30),
                        CP_MethodDescription = c.String(maxLength: 100),
                    })
                .PrimaryKey(t => t.CP_ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ConversionProcesses");
        }
    }
}
