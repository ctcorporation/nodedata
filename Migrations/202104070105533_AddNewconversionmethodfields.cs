﻿namespace NodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddNewconversionmethodfields : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PROFILE", "P_CONVFROMMETHOD", c => c.String(maxLength: 3));
            AddColumn("dbo.PROFILE", "P_CONVTOMETHOD", c => c.String(maxLength: 3));
            AddColumn("dbo.PROFILE", "P_CONVFROMMETHODNAME", c => c.Guid(nullable: false));
            AddColumn("dbo.PROFILE", "P_CONVTOMETHODNAME", c => c.Guid(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PROFILE", "P_CONVTOMETHODNAME");
            DropColumn("dbo.PROFILE", "P_CONVFROMMETHODNAME");
            DropColumn("dbo.PROFILE", "P_CONVTOMETHOD");
            DropColumn("dbo.PROFILE", "P_CONVFROMMETHOD");
        }
    }
}
