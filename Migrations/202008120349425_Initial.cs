﻿namespace NodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.ApiLogs",
            //    c => new
            //        {
            //            F_TRACKINGID = c.Guid(nullable: false, identity: true),
            //            F_FILENAME = c.String(nullable: false, maxLength: 80),
            //            F_SENDERID = c.String(nullable: false, maxLength: 15),
            //            F_CLIENTID = c.String(maxLength: 20),
            //            F_APPCODE = c.String(maxLength: 3),
            //            F_SUBJECT = c.String(maxLength: 50),
            //            F_CWFILENAME = c.String(maxLength: 80),
            //            F_DATERECEIVED = c.DateTime(),
            //        })
            //    .PrimaryKey(t => t.F_TRACKINGID);
            
            //CreateTable(
            //    "dbo.Archives",
            //    c => new
            //        {
            //            AL_ID = c.Guid(nullable: false, identity: true),
            //            AL_TL = c.Guid(nullable: false),
            //            AL_Path = c.String(maxLength: 50),
            //            AL_ArchiveName = c.String(maxLength: 20),
            //            AL_DateArchived = c.DateTime(nullable: false),
            //            AL_FileName = c.String(maxLength: 50),
            //        })
            //    .PrimaryKey(t => t.AL_ID);
            
            //CreateTable(
            //    "dbo.Cargowise_Enums",
            //    c => new
            //        {
            //            CW_ID = c.Guid(nullable: false, identity: true),
            //            CW_ENUMTYPE = c.String(maxLength: 20, fixedLength: true, unicode: false),
            //            CW_ENUM = c.String(maxLength: 10, fixedLength: true),
            //            CW_MAPVALUE = c.String(maxLength: 20, unicode: false),
            //        })
            //    .PrimaryKey(t => t.CW_ID);
            
            //CreateTable(
            //    "dbo.CustomerModuleLookups",
            //    c => new
            //        {
            //            MC_ID = c.Guid(nullable: false, identity: true),
            //            MC_C = c.Guid(),
            //            MC_MO = c.Guid(),
            //        })
            //    .PrimaryKey(t => t.MC_ID)
            //    .ForeignKey("dbo.Customer", t => t.MC_C)
            //    .ForeignKey("dbo.ModuleListings", t => t.MC_MO)
            //    .Index(t => t.MC_C)
            //    .Index(t => t.MC_MO);
            
            //CreateTable(
            //    "dbo.Customer",
            //    c => new
            //        {
            //            C_ID = c.Guid(nullable: false, identity: true),
            //            C_NAME = c.String(maxLength: 50, unicode: false),
            //            C_IS_ACTIVE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_ON_HOLD = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_PATH = c.String(maxLength: 80, unicode: false),
            //            C_FTP_CLIENT = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_CODE = c.String(maxLength: 15, unicode: false),
            //            C_TRIAL = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_TRIALSTART = c.DateTime(),
            //            C_TRIALEND = c.DateTime(),
            //            C_SHORTNAME = c.String(maxLength: 10, fixedLength: true),
            //            C_SATELLITE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_INVOICED = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //        })
            //    .PrimaryKey(t => t.C_ID);
            
            //CreateTable(
            //    "dbo.HEARTBEAT",
            //    c => new
            //        {
            //            HB_ID = c.Guid(nullable: false, identity: true),
            //            HB_NAME = c.String(maxLength: 50),
            //            HB_PATH = c.String(maxLength: 200),
            //            HB_ISMONITORED = c.String(nullable: false, maxLength: 1),
            //            HB_PID = c.Int(),
            //            HB_LASTOPERATION = c.String(maxLength: 50),
            //            HB_LASTCHECKIN = c.DateTime(),
            //            HB_OPENED = c.DateTime(),
            //            HB_LASTOPERATIONPARAMS = c.String(maxLength: 100),
            //            HB_C = c.Guid(),
            //        })
            //    .PrimaryKey(t => t.HB_ID)
            //    .ForeignKey("dbo.Customer", t => t.HB_C)
            //    .Index(t => t.HB_C);
            
            //CreateTable(
            //    "dbo.ModuleListings",
            //    c => new
            //        {
            //            MO_ID = c.Guid(nullable: false, identity: true),
            //            MO_ModuleName = c.String(maxLength: 20),
            //            MO_Chargeable = c.Boolean(nullable: false),
            //            MO_ModuleDescription = c.String(maxLength: 100),
            //        })
            //    .PrimaryKey(t => t.MO_ID);
            
            //CreateTable(
            //    "dbo.DTS",
            //    c => new
            //        {
            //            D_ID = c.Guid(nullable: false, identity: true),
            //            D_C = c.Guid(),
            //            D_INDEX = c.Int(),
            //            D_FINALPROCESSING = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            D_P = c.Guid(),
            //            D_FILETYPE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            D_DTSTYPE = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            D_DTS = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            D_SEARCHPATTERN = c.String(unicode: false),
            //            D_NEWVALUE = c.String(unicode: false),
            //            D_QUALIFIER = c.String(unicode: false),
            //            D_TARGET = c.String(unicode: false),
            //            D_CURRENTVALUE = c.String(unicode: false),
            //        })
            //    .PrimaryKey(t => t.D_ID);
            
            //CreateTable(
            //    "dbo.MapHeaders",
            //    c => new
            //        {
            //            MH_ID = c.Guid(nullable: false, identity: true),
            //            MH_Name = c.String(),
            //            MH_Description = c.String(),
            //            MH_MO = c.Guid(nullable: false),
            //            MH_Customer = c.String(maxLength: 20),
            //            MH_Recipient = c.String(maxLength: 20),
            //        })
            //    .PrimaryKey(t => t.MH_ID);
            
            //CreateTable(
            //    "dbo.MapOperations",
            //    c => new
            //        {
            //            MD_ID = c.Guid(nullable: false, identity: true),
            //            MD_MapDescription = c.String(nullable: false, maxLength: 100),
            //            MD_FromField = c.String(maxLength: 50),
            //            MD_ToField = c.String(maxLength: 50),
            //            MD_DataType = c.String(maxLength: 3),
            //            MD_MH = c.Guid(),
            //        })
            //    .PrimaryKey(t => t.MD_ID);
            
            //CreateTable(
            //    "dbo.MappingMasterFields",
            //    c => new
            //        {
            //            MM_ID = c.Guid(nullable: false, identity: true),
            //            MM_FieldName = c.String(maxLength: 50),
            //            MM_MO = c.Guid(nullable: false),
            //            MM_FieldType = c.String(maxLength: 4),
            //        })
            //    .PrimaryKey(t => t.MM_ID);
            
            //CreateTable(
            //    "dbo.MappingMasterOperations",
            //    c => new
            //        {
            //            MO_ID = c.Guid(nullable: false, identity: true),
            //            MO_OperationName = c.String(maxLength: 50),
            //            MO_Description = c.String(),
            //            MD_MethodName = c.String(maxLength: 50),
            //        })
            //    .PrimaryKey(t => t.MO_ID);
            
            //CreateTable(
            //    "dbo.Processing_Error",
            //    c => new
            //        {
            //            E_PK = c.Guid(nullable: false, identity: true),
            //            E_SENDERID = c.String(maxLength: 15),
            //            E_RECIPIENTID = c.String(maxLength: 15),
            //            E_PROCDATE = c.DateTime(),
            //            E_FILENAME = c.String(),
            //            E_ERRORDESC = c.String(),
            //            E_ERRORCODE = c.String(maxLength: 10),
            //            E_P = c.Guid(),
            //            E_IGNORE = c.String(nullable: false, maxLength: 1),
            //        })
            //    .PrimaryKey(t => t.E_PK);
            
            //CreateTable(
            //    "dbo.PROFILE",
            //    c => new
            //        {
            //            P_ID = c.Guid(nullable: false, identity: true),
            //            P_C = c.Guid(),
            //            P_REASONCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_SERVER = c.String(unicode: false),
            //            P_USERNAME = c.String(unicode: false),
            //            P_PASSWORD = c.String(unicode: false),
            //            P_DELIVERY = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_PORT = c.String(maxLength: 10, fixedLength: true, unicode: false),
            //            P_DESCRIPTION = c.String(unicode: false),
            //            P_PATH = c.String(maxLength: 100, unicode: false),
            //            P_DIRECTION = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_LIBNAME = c.String(maxLength: 50, unicode: false),
            //            P_MESSAGETYPE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_RECIPIENTID = c.String(maxLength: 15, unicode: false),
            //            P_MSGTYPE = c.String(maxLength: 20, fixedLength: true, unicode: false),
            //            P_CHARGEABLE = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_BILLTO = c.String(maxLength: 15, fixedLength: true, unicode: false),
            //            P_SENDERID = c.String(maxLength: 15, unicode: false),
            //            P_DTS = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_EMAILADDRESS = c.String(maxLength: 100, unicode: false),
            //            P_ACTIVE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_SSL = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_SENDEREMAIL = c.String(maxLength: 100, unicode: false),
            //            P_SUBJECT = c.String(maxLength: 100, unicode: false),
            //            P_FILETYPE = c.String(maxLength: 7, fixedLength: true, unicode: false),
            //            P_MESSAGEDESCR = c.String(maxLength: 20, unicode: false),
            //            P_EVENTCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_CUSTOMERCOMPANYNAME = c.String(maxLength: 50, unicode: false),
            //            P_GROUPCHARGES = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_XSD = c.String(maxLength: 50, unicode: false),
            //            P_PARAMLIST = c.String(unicode: false),
            //            P_METHOD = c.String(maxLength: 50, unicode: false),
            //            P_NOTIFY = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_FORWARDWITHFLAGS = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_CWSUBJECT = c.String(maxLength: 80),
            //            P_CWFILENAME = c.String(maxLength: 80),
            //            P_CWAPPCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_RENAMEFILE = c.Boolean(nullable: false),
            //        })
            //    .PrimaryKey(t => t.P_ID);
            
            //CreateTable(
            //    "dbo.TaskLists",
            //    c => new
            //        {
            //            TL_ID = c.Guid(nullable: false, identity: true),
            //            TL_OrginalFileName = c.String(maxLength: 100),
            //            TL_FileName = c.String(maxLength: 50),
            //            TL_ReceiveDate = c.DateTime(),
            //            TL_Processed = c.Boolean(nullable: false),
            //            TL_Path = c.String(maxLength: 150),
            //            TL_Subject = c.String(maxLength: 100),
            //            TL_SenderID = c.String(maxLength: 50),
            //            TL_RecipientID = c.String(maxLength: 50),
            //            TL_ReceivedFolder = c.String(maxLength: 150),
            //            TL_ReceiverProcess = c.String(maxLength: 10),
            //            TL_ProcessDate = c.DateTime(),
            //            TL_P = c.Guid(),
            //            TL_CWFileName = c.String(maxLength: 80),
            //            TL_FromAddress = c.String(maxLength: 50),
            //            TL_ToAddress = c.String(maxLength: 50),
            //            TL_ArchiveName = c.String(maxLength: 200),
            //        })
            //    .PrimaryKey(t => t.TL_ID);
            
            //CreateTable(
            //    "dbo.TODO",
            //    c => new
            //        {
            //            L_ID = c.Guid(nullable: false, identity: true),
            //            L_P = c.Guid(),
            //            L_FILENAME = c.String(),
            //            L_LASTRESULT = c.String(),
            //            L_DATE = c.DateTime(),
            //        })
            //    .PrimaryKey(t => t.L_ID);
            
            //CreateTable(
            //    "dbo.TRANSACTION_LOG",
            //    c => new
            //        {
            //            X_ID = c.Guid(nullable: false, identity: true),
            //            X_P = c.Guid(),
            //            X_FILENAME = c.String(maxLength: 150),
            //            X_DATE = c.DateTime(),
            //            X_SUCCESS = c.String(nullable: false, maxLength: 1),
            //            X_C = c.Guid(),
            //            X_LASTRESULT = c.String(maxLength: 120),
            //        })
            //    .PrimaryKey(t => t.X_ID);
            
            //CreateTable(
            //    "dbo.Transactions",
            //    c => new
            //        {
            //            T_ID = c.Guid(nullable: false, identity: true),
            //            T_C = c.Guid(),
            //            T_P = c.Guid(),
            //            T_DATETIME = c.DateTime(),
            //            T_FILENAME = c.String(maxLength: 255),
            //            T_TRIAL = c.String(nullable: false, maxLength: 1),
            //            T_INVOICED = c.String(nullable: false, maxLength: 1),
            //            T_INVOICEDATE = c.DateTime(),
            //            T_MSGTYPE = c.String(maxLength: 10),
            //            T_BILLTO = c.String(maxLength: 15),
            //            T_DIRECTION = c.String(maxLength: 1),
            //            T_CHARGEABLE = c.String(maxLength: 1),
            //            T_REF1 = c.String(maxLength: 200),
            //            T_REF2 = c.String(maxLength: 200),
            //            T_REF3 = c.String(maxLength: 200),
            //            T_ARCHIVE = c.String(maxLength: 100),
            //            T_REF1TYPE = c.String(maxLength: 20),
            //            T_REF2TYPE = c.String(maxLength: 20),
            //            T_REF3TYPE = c.String(maxLength: 20),
            //            T_PURPOSE = c.String(maxLength: 3),
            //            T_EVENT = c.String(maxLength: 3),
            //        })
            //    .PrimaryKey(t => t.T_ID);
            
            //CreateTable(
            //    "dbo.vw_CustomerProfile",
            //    c => new
            //        {
            //            C_IS_ACTIVE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_ON_HOLD = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_FTP_CLIENT = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_ID = c.Guid(nullable: false),
            //            C_ID = c.Guid(nullable: false),
            //            P_DIRECTION = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_DTS = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_ACTIVE = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            P_NOTIFY = c.String(nullable: false, maxLength: 1, fixedLength: true, unicode: false),
            //            C_NAME = c.String(maxLength: 50, unicode: false),
            //            C_CODE = c.String(maxLength: 15, unicode: false),
            //            C_PATH = c.String(maxLength: 80, unicode: false),
            //            P_C = c.Guid(),
            //            P_REASONCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_SERVER = c.String(unicode: false),
            //            P_USERNAME = c.String(unicode: false),
            //            P_PASSWORD = c.String(unicode: false),
            //            P_DESCRIPTION = c.String(unicode: false),
            //            P_PORT = c.String(maxLength: 10, fixedLength: true, unicode: false),
            //            P_DELIVERY = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_PATH = c.String(maxLength: 100, unicode: false),
            //            P_XSD = c.String(maxLength: 50, unicode: false),
            //            P_LIBNAME = c.String(maxLength: 50, unicode: false),
            //            P_RECIPIENTID = c.String(maxLength: 15, unicode: false),
            //            P_SENDERID = c.String(maxLength: 15, unicode: false),
            //            P_BILLTO = c.String(maxLength: 15, fixedLength: true, unicode: false),
            //            P_CHARGEABLE = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_MSGTYPE = c.String(maxLength: 20, fixedLength: true, unicode: false),
            //            P_MESSAGETYPE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_FILETYPE = c.String(maxLength: 7, fixedLength: true, unicode: false),
            //            P_EMAILADDRESS = c.String(maxLength: 100, unicode: false),
            //            P_SSL = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_SENDEREMAIL = c.String(maxLength: 100, unicode: false),
            //            P_SUBJECT = c.String(maxLength: 100, unicode: false),
            //            P_MESSAGEDESCR = c.String(maxLength: 20, unicode: false),
            //            P_EVENTCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_CUSTOMERCOMPANYNAME = c.String(maxLength: 50, unicode: false),
            //            C_SHORTNAME = c.String(maxLength: 10, fixedLength: true),
            //            P_METHOD = c.String(maxLength: 50, unicode: false),
            //            P_PARAMLIST = c.String(unicode: false),
            //            P_FORWARDWITHFLAGS = c.String(maxLength: 1, fixedLength: true, unicode: false),
            //            P_CWSUBJECT = c.String(maxLength: 80),
            //            P_CWFILENAME = c.String(maxLength: 80),
            //            P_CWAPPCODE = c.String(maxLength: 3, fixedLength: true, unicode: false),
            //            P_GROUPCHARGES = c.String(maxLength: 1),
            //            P_RENAMEFILE = c.Boolean(nullable: false),
            //        })
            //    .PrimaryKey(t => new { t.C_IS_ACTIVE, t.C_ON_HOLD, t.C_FTP_CLIENT, t.P_ID, t.C_ID, t.P_DIRECTION, t.P_DTS, t.P_ACTIVE, t.P_NOTIFY });
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CustomerModuleLookups", "MC_MO", "dbo.ModuleListings");
            DropForeignKey("dbo.CustomerModuleLookups", "MC_C", "dbo.Customer");
            DropForeignKey("dbo.HEARTBEAT", "HB_C", "dbo.Customer");
            DropIndex("dbo.HEARTBEAT", new[] { "HB_C" });
            DropIndex("dbo.CustomerModuleLookups", new[] { "MC_MO" });
            DropIndex("dbo.CustomerModuleLookups", new[] { "MC_C" });
            DropTable("dbo.vw_CustomerProfile");
            DropTable("dbo.Transactions");
            DropTable("dbo.TRANSACTION_LOG");
            DropTable("dbo.TODO");
            DropTable("dbo.TaskLists");
            DropTable("dbo.PROFILE");
            DropTable("dbo.Processing_Error");
            DropTable("dbo.MappingMasterOperations");
            DropTable("dbo.MappingMasterFields");
            DropTable("dbo.MapOperations");
            DropTable("dbo.MapHeaders");
            DropTable("dbo.DTS");
            DropTable("dbo.ModuleListings");
            DropTable("dbo.HEARTBEAT");
            DropTable("dbo.Customer");
            DropTable("dbo.CustomerModuleLookups");
            DropTable("dbo.Cargowise_Enums");
            DropTable("dbo.Archives");
            DropTable("dbo.ApiLogs");
        }
    }
}
