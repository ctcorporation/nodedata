﻿// <auto-generated />
namespace NodeData.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.3.0")]
    public sealed partial class AddAppLog : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddAppLog));
        
        string IMigrationMetadata.Id
        {
            get { return "202008250309211_Add.AppLog"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
