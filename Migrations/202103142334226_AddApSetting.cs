﻿namespace NodeData.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddApSetting : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AppSettings",
                c => new
                {
                    AS_Id = c.Guid(nullable: false, identity: true, defaultValueSql: "newid()"),
                    AS_AppKey = c.Guid(nullable: false),
                    AS_AcceptedFiles = c.String(),
                    AS_RejectFiles = c.String(),
                    AS_RejectLocation = c.String(),
                    AS_ArchiveLocation = c.String(),
                    AS_ArchiveDays = c.Int(nullable: false),
                })
                .PrimaryKey(t => t.AS_Id);

        }

        public override void Down()
        {
            DropTable("dbo.AppSettings");
        }
    }
}
