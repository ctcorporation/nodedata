﻿namespace NodeData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class profilesaddprofilenotes : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PROFILE", "P_NOTES", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PROFILE", "P_NOTES");
        }
    }
}
