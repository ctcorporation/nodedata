﻿using NodeData.Models;

namespace NodeData
{
    public interface IProfileRepository : IGenericRepository<Profile>
    { }


}