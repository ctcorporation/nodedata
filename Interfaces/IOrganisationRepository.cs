﻿using NodeData.Models;

namespace NodeData.Interfaces
{
    public interface IOrganisationRepository : IGenericRepository<Organisation>
    {
        TransModContext TransModContext { get; }
    }
}
