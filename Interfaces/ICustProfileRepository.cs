﻿using NodeData.Models;

namespace NodeData
{
    public interface ICustProfileRepository : IGenericRepository<vw_CustomerProfile>
    {

    }
}