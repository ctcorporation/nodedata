﻿using NodeData.Models;

namespace NodeData
{
    public interface ITaskListsRepository : IGenericRepository<TaskList>
    {

    }
}