﻿using NodeData.Models;

namespace NodeData.Repository
{
    public interface ITransLogRepository : IGenericRepository<TransactionLog>
    {
        NodeDataContext NodeDataContext { get; }
    }
}