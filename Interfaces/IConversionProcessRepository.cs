﻿using NodeData.Models;

namespace NodeData.Repository
{
    public interface IConversionProcessRepository : IGenericRepository<ConversionProcess>
    {
        NodeDataContext NodeDataContext { get; }
    }
}