﻿using NodeData.Models;

namespace NodeData
{
    public interface IMapHeaderRepository : IGenericRepository<MapHeader>
    {
    }
}
