﻿using NodeData.Models;

namespace NodeData.Interfaces
{
    public interface IEnumRepository : IGenericRepository<Enums>
    {
        TransModContext TransModContext { get; }
    }
}