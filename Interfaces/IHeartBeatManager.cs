﻿using NodeData.Models;
using System;
using System.Reflection;

namespace NodeData
{
    public interface IHeartBeatManager : IDisposable
    {
        string ConnString { get; set; }
        string CustCode { get; set; }
        string ExeName { get; set; }
        string ExePath { get; set; }
        void RegisterHeartBeat(string custCode, string operation, ParameterInfo[] parameters);

        HeartBeat GetHeartBeat(Guid hbId);
    }
}