﻿using NodeData.Models;

namespace NodeData.Interfaces
{
    public interface IRunLinkRepository : IGenericRepository<RunLink>
    {
        TransModContext TransModContext { get; }
    }
}
