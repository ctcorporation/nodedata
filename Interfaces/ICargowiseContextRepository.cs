﻿using NodeData.Models;

namespace NodeData
{
    public interface ICargowiseContextRepository : IGenericRepository<CargowiseContext>
    {
        NodeDataContext NodeDataContext { get; }
    }
}