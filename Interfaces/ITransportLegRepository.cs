﻿using NodeData.Models;

namespace NodeData.Interfaces
{
    public interface ITransportLegRepository : IGenericRepository<TransportLeg>
    {
        TransModContext TransModContext { get; }
    }
}
