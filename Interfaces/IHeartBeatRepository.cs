﻿using NodeData.Models;

namespace NodeData
{
    public interface IHeartBeatRepository : IGenericRepository<HeartBeat>
    {
        NodeDataContext NodeDataContext { get; }
    }
}