﻿using NodeData.Models;

namespace NodeData.Repository
{
    public interface IAppLogRepository : IGenericRepository<AppLog>
    {
        NodeDataContext NodeDataContext { get; }
    }
}