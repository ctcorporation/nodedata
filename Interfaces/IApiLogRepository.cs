﻿using NodeData.Models;

namespace NodeData
{
    public interface IApiLogRepository : IGenericRepository<ApiLog>
    {
        NodeDataContext NodeDataContext { get; }
    }
}