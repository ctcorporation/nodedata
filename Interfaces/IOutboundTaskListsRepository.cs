﻿using NodeData.Models;

namespace NodeData
{
    public interface IOutboundTaskListsRepository : IGenericRepository<OutboundTaskList>
    {

    }
}