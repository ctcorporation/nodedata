﻿using NodeData.Models;

namespace NodeData
{
    public interface IModuleListingRepository : IGenericRepository<ModuleListing>
    {
        NodeDataContext NodeDataContext { get; }
    }
}