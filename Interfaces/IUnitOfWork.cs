﻿using NodeData.Interfaces;
using NodeData.Repository;
using System;
using System.Threading.Tasks;

namespace NodeData
{
    public interface IUnitOfWork : IDisposable
    {
        string ErrorMessage { get; }

        IProcErrorRepository ProcErrors { get; }
        ITransLogRepository TransLogs { get; }
        IToDoRepository ToDos { get; }
        IMapHeaderRepository MapHeaders { get; }
        IMappingRepository MapOperations { get; }
        IMappingMasterFieldRepository MappingMasterFields { get; }
        IMappingMasterOperationRepository MappingMasterOperations { get; }
        IModuleListingRepository Modules { get; }
        ICustomerModuleRepository CustomerModules { get; }
        IDTSRepository DTServices { get; }
        IApiLogRepository ApiLogs { get; }
        ICustProfileRepository CustProfiles { get; }
        ITransactionRepository Transactions { get; }
        ICargowise_EnumsRepository CargowiseEnums { get; }
        ICustomerRepository Customers { get; }
        IProfileRepository Profiles { get; }
        ITaskListsRepository TaskLists { get; }
        IOutboundTaskListsRepository OutboundTaskLists { get; }
        IHeartBeatRepository HeartBeats { get; }
        IAddressRepository Addresses { get; }
        IEnumRepository TransEnums { get; }
        IEnumTypesRepository TransEnumTypes { get; }
        IImageRepository Images { get; }
        IJobHeaderRepository JobHeaders { get; }
        IJobNoteRepository JobNotes { get; }
        IOrganisationRepository Organisations { get; }
        IRunLinkRepository RunLinks { get; }
        IRunSheetRepository RunSheets { get; }
        ITransportLegRepository TransportLegs { get; }
        IAppLogRepository AppLogs { get; }
        IAppSettingRepository AppSettings { get; }
        IConversionProcessRepository ConversionProcesses { get;  }

        IReferenceNumbersRepository ReferenceNumbers { get; }
        IOrderRepository Orders { get; }
        IOrderLineRepository OrderLines { get; }

        bool DBExists();
        Task<int> CompleteAsync();
        int Complete();

    }
}