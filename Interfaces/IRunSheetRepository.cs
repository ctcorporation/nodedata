﻿using NodeData.Models;

namespace NodeData.Interfaces
{
    public interface IRunSheetRepository : IGenericRepository<RunSheet>
    {
        TransModContext TransModContext { get; }
    }
}
