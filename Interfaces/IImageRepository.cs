﻿using NodeData.Models;

namespace NodeData.Interfaces
{
    public interface IImageRepository : IGenericRepository<Image>
    {
        TransModContext TransModContext { get; }
    }
}
