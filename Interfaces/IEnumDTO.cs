﻿using NodeData.Models;

namespace NodeData
{
    public interface IEnumDTO
    {
        NodeDataContext Context { get; set; }

        string GetEnum(string enumType, string mapValue);
        string GetMapValue(string enumType, string eNum);
    }
}