﻿using NodeData.Models;

namespace NodeData
{
    public interface ICustomerModuleRepository : IGenericRepository<CustomerModuleLookup>
    {
        NodeDataContext NodeDataContext { get; }
    }
}