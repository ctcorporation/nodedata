﻿using NodeData.Models;

namespace NodeData.Repository
{
    public interface IDTSRepository : IGenericRepository<DTS>
    {
        NodeDataContext NodeDataContext { get; }
    }
}