﻿using NodeData.Models;

namespace NodeData.Repository
{
    public interface IToDoRepository : IGenericRepository<ToDo>
    {
        NodeDataContext NodeDataContext { get; }
    }
}