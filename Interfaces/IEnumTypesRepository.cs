﻿using NodeData.Models;

namespace NodeData.Interfaces
{
    public interface IEnumTypesRepository : IGenericRepository<EnumType>
    {
        TransModContext TransModContext { get; }
    }
}
