﻿using NodeData.Models;

namespace NodeData.Repository
{
    public interface IMappingMasterFieldRepository : IGenericRepository<MappingMasterField>
    {

    }
}