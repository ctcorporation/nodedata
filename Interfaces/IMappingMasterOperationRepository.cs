﻿using NodeData.Models;

namespace NodeData.Repository
{
    public interface IMappingMasterOperationRepository : IGenericRepository<MappingMasterOperation>
    {

    }
}