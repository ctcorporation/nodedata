﻿using NodeData.Models;

namespace NodeData.Interfaces
{
    public interface IJobHeaderRepository : IGenericRepository<JobHeader>
    {
        TransModContext TransModContext
        {
            get;
        }
    }
}
