﻿using NodeData.Models;

namespace NodeData
{
    public interface ITransactionRepository : IGenericRepository<Transaction>
    {

    }
}