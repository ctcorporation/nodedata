﻿using NodeData.Models;

namespace NodeData
{
    public interface IReferenceNumbersRepository : IGenericRepository<ReferenceNumber>
    {

    }
}