﻿using System;

namespace NodeData
{
    public interface ICustomerFunctions
    {
        string ConnString { get; set; }

        Guid? GetCustId(string custCode);
    }
}