﻿using NodeData.Models;

namespace NodeData.Interfaces
{
    public interface IJobNoteRepository : IGenericRepository<JobNote>
    {
        TransModContext TransModContext { get; }
    }
}
