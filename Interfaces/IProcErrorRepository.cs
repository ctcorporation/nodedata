﻿using NodeData.Models;

namespace NodeData.Repository
{
    public interface IProcErrorRepository : IGenericRepository<Processing_Error>
    {
        NodeDataContext NodeDataContext { get; }
    }
}