﻿using NodeData.Models;

namespace NodeData
{
    public interface ICargowise_EnumsRepository : IGenericRepository<Cargowise_Enums>
    {
        NodeDataContext NodeDataContext { get; }
    }
}