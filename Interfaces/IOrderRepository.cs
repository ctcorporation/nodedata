﻿using NodeData.Models;

namespace NodeData
{
    public interface IOrderRepository : IGenericRepository<CTCOrder>
    {
    }
}