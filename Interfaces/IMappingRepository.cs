﻿using NodeData.Models;

namespace NodeData
{
    public interface IMappingRepository : IGenericRepository<MapOperation>
    {
    }
}
