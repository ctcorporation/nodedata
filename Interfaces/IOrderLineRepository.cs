﻿using NodeData.Models;

namespace NodeData
{

    public interface IOrderLineRepository : IGenericRepository<CTCOrderline>
    {
    }
}