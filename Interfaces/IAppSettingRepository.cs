﻿using NodeData.Models;

namespace NodeData.Repository
{
    public interface IAppSettingRepository : IGenericRepository<AppSetting>
    {
        NodeDataContext NodeDataContext { get; }
    }
}