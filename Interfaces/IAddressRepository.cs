﻿using NodeData.Models;

namespace NodeData.Interfaces
{
    public interface IAddressRepository : IGenericRepository<Address>
    {
        TransModContext TransModContext { get; }
    }
}