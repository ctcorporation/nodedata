﻿
using NodeData.Classes;
using NodeData.DTO;
using NodeData.Models;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;

namespace NodeData
{
    public class HeartBeatManager : IHeartBeatManager
    {
        #region Members
        private string _errorMessage = string.Empty;
        private string _connString;
        #endregion

        #region Properties
        public string ConnString
        {
            get { return _connString; }
            set { _connString = value; }
        }

        public string CustCode
        {
            get; set;
        }

        public string ExeName { get; set; }
        public string ExePath { get; set; }
        #endregion

        #region Constructors
        public HeartBeatManager(string connString)
        {
            ConnString = connString;
        }
        #endregion

        #region Methods
        public void RegisterHeartBeat(string custCode, string operation, ParameterInfo[] parameters)
        {
            try
            {
                using (NodeData.IUnitOfWork uow = new UnitOfWork(new NodeDataContext(_connString)))
                {
                    var _exeName = string.IsNullOrEmpty(ExeName) ? Assembly.GetExecutingAssembly().GetName().Name : ExeName;
                    NodeData.Models.HeartBeat heartBeat = new NodeData.Models.HeartBeat();
                    ICustomerFunctions custFunc = new CustomerFunctions(_connString);
                    var cust = custFunc.GetCustId(custCode);
                    heartBeat = uow.HeartBeats.Find(x => x.HB_C == cust && x.HB_NAME == _exeName).FirstOrDefault();

                    bool newHeartBeat = false;

                    if (heartBeat == null)
                    {
                        heartBeat = new NodeData.Models.HeartBeat
                        {
                            HB_ID = Guid.NewGuid(),
                            HB_C = cust,
                            HB_NAME = _exeName

                        };
                        newHeartBeat = true;
                    }
                    heartBeat.HB_PATH = string.IsNullOrEmpty(ExePath) ? Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                    System.Reflection.Assembly.GetExecutingAssembly().GetName().Name) : ExePath;
                    heartBeat.HB_ISMONITORED = "Y";
                    heartBeat.HB_LASTCHECKIN = DateTime.Now;
                    heartBeat.HB_LASTOPERATION = operation;
                    string pList = string.Empty;
                    if (parameters != null)
                    {
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            pList += parameters[i].Name + ": " + parameters[i].ToString();
                        }
                    }
                    heartBeat.HB_LASTOPERATIONPARAMS = pList;
                    if (operation == "Starting")
                    {
                        heartBeat.HB_OPENED = DateTime.Now;
                        Process currentProcess = Process.GetCurrentProcess();
                        heartBeat.HB_PID = currentProcess.Id;
                    }
                    if (operation == "Stopping")
                    {

                        heartBeat.HB_OPENED = null;
                        heartBeat.HB_LASTCHECKIN = null;
                        heartBeat.HB_PID = 0;

                    }
                    if (newHeartBeat)
                    {
                        uow.HeartBeats.Add(heartBeat);
                    }

                    uow.Complete();
                }
            }

            catch (Exception ex)
            {
                string strEx = ex.GetType().Name;
                var st = new StackTrace();
                var sf = st.GetFrame(0);
                Console.WriteLine("Ex: " + ex.Message + " Stack:" + ex.StackTrace + " Inner: " + ex.InnerException.Message);

            }

        }

        #endregion

        #region Helpers
        public HeartBeat GetHeartBeat(Guid custID)

        {
            var hbId = ReturnSatelliteId(custID);
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(_connString)))
            {
                var hb = uow.HeartBeats.Get(hbId);
                if (hb != null)
                {
                    return hb;
                }
            }
            return null;
        }
        public Guid ReturnSatelliteId(Guid custId)
        {
            using (IUnitOfWork uow = new UnitOfWork(new NodeDataContext(_connString)))
            {
                var hb = (uow.HeartBeats.Find(x => x.HB_C == custId)).FirstOrDefault();
                if (hb != null)
                {
                    return (Guid)hb.HB_C;
                }
                return Guid.Empty;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~HeartBeatManager()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
        #endregion


    }
}
