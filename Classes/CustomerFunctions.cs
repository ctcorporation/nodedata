﻿using NodeData.DTO;
using System;
using System.Linq;

namespace NodeData.Classes
{
    public class CustomerFunctions : ICustomerFunctions
    {
        #region Members
        string _connString;
        string _errorMessage;
        #endregion

        #region Constructor
        public CustomerFunctions(string connString)
        {
            ConnString = connString;
        }
        #endregion

        #region Properties
        public string ConnString
        {
            get
            {
                return _connString;
            }
            set
            {
                _connString = value;
            }
        }
        #endregion

        #region Methods
        public Guid? GetCustId(string custCode)
        {
            using (IUnitOfWork uow = new UnitOfWork(new Models.NodeDataContext(_connString)))
            {
                var cust = (uow.Customers.Find(x => x.C_CODE == custCode)).FirstOrDefault();
                return cust == null ? Guid.Empty : cust.C_ID;
            };
        }
        #endregion
    }
}
